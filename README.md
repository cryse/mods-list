﻿[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_mods-list&metric=alert_status)](https://sonarcloud.io/dashboard?id=wot-public-mods_mods-list) 
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_mods-list&metric=ncloc)](https://sonarcloud.io/dashboard?id=wot-public-mods_mods-list)
[![Visits](https://gitlab.poliroid.ru/api/badge/mods-list/visits)](https://gitlab.com/wot-public-mods/mods-list)
[![Downloads](https://gitlab.poliroid.ru/api/badge/mods-list/downloads)](https://gitlab.com/wot-public-mods/mods-list/-/releases)
[![Donate](https://cdn.poliroid.ru/gitlab/donate.svg)](https://poliroid.ru/donate)

**ModsList** This is a modification for the game "World Of Tanks" which allows you to assemble the modifications in one place.

### What do you need it for?
* **ModsList** This is the place where start visual mods (Mods configurator, WGFM Radio, Replays manager, and many more goodies)
* **ModsList** It is flexible and common for all APIs

### Use
* Add a distribution to your mod
* Send information about your modification using the API

### Example of what a menu looks like in a hangar
![Example of what a menu looks like in a hangar](https://static.poliroid.ru/modsListApi_lobby.jpg)


### Example of how the menu looks in the login window
![Example of how the menu looks in the login window](https://static.poliroid.ru/modsListApi_login.jpg)
